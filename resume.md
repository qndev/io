---
layout: page
title: RÉSUMÉ
permalink: /resume/
---

##### Personal Data

* Place and Date of Birth: Thai Binh, Viet Nam - 15th April 1996
* Address: Tan Mai, Hoang Mai, Ha Noi
* Phone: (+84) 975 857 109
* Email: quangnd.hust@gmail.com

##### Work Experience

* Sep-Dec 2017 - On the Job Training at Fpt Software, Hanoi

##### Education

* 2014 - Present Hanoi University of Science and Technology      

##### Technical Skills

* Databases:
   * MySQL, SQL Server, MongoDB
* Operating Systems:
   *  Linux (Ubuntu, Debian)
* Languages:
   * HTML, CSS, Java, JavaScript
* Version control:
   * Git
