---
layout: page
title: Portfolio
permalink: /portfolio/
---

### Một số Projects tôi đã làm.

##### CRUD Operations SAP-UI5 

* Crud, SapUI5, Odata-service, Sap-cloud-platform
* [View on GitHub](https://github.com/quangndhust/crud-operations-sap-ui5)

<div class="divider"></div>

##### Thiết kế xây dựng phần mềm quản lý thông tin sinh viên

* Java, UML, SRS, SDD
* [View on GitHub](https://github.com/quangndhust/tkxdpm.20181)

<div class="divider"></div>

##### Shopping Cart

* NodeJs, MongDB, Stripe
* [View on GitHub](https://github.com/quangndhust/shopping-cart-v_2)

<div class="divider"></div>

##### Sapfiori

* SapUI5, Fioriapp
* [View on GitHub](https://github.com/quangndhust/sapfiori)
